partialcorrect3=0;
for i=2:size(results3,1)
    numofguesses=floor(size(results3{i,4},2)/2);
    predict=results3{i,4}(1:numofguesses);
    if(isequal(results3{i,5},'no') && sum(ismember(predict,results3{i,2}))>0)
        partialcorrect3=partialcorrect3+1;
    end
end