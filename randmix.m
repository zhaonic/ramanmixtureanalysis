function [mixture,mixlist,weights] = randmix(n, NarcoticLibrary,g)
%g is derivative matrix for Savitsky Golay degree=3, frame=15
%generates a random linear combination of n chemicals in
%NarcoticLibrary2ndD_N
mixlist=randcombo(n)+1;
% weights=zeros(n,1);
% weights(1)=0.1+rand*(1-n*0.1);
% for i=2:n-1
%     weights(i)=0.1+rand*(1-sum(weights(1:i-1))-0.1*(n-i+1));
% end
% weights(n)=1-sum(weights(1:n-1));

weights=rand(n,1);
weights=weights/norm(weights,1);
while(min(weights)<0.1)
    weights=rand(n,1);
    weights=weights/norm(weights,1);
end
%was for project Phase 1
%mixture=NL2ndD_Nnormed(12:461,mixlist)*weights;
%Phase 2 uses the original spectra in NarcoticLibrary
mixture=NarcoticLibrary(5:468,mixlist)*weights;
%Add Poisson (approximately Gaussian) shot noise to each wavenumber
%(horizontal axis) of original signal. Assuming large number of photons for
%each wavenumber, let I be the intensity at a certain wavenumber, we can
%approximate the Poisson-distributed signal with Normal(I, sqrt(I)) by
%Central Limit Theorem
%Since 1 electron of signal may in general represent c photons, we make the
%variance at each wavenumber cI instead of just I.
c=1;
for i=1:464
    mixture(i)=normrnd(mixture(i),sqrt(c*mixture(i)));
end
mixture=conv(mixture,factorial(2)/(-4)^2*g(:,3),'valid');
end
