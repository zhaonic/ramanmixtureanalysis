function [mixlisttry] = mixdecompfocushigher(mixture, NL2ndD_Nnormed)
%this alternate version of mixdecomp will: As soon as find a combination that
%fits higher threshold of any layer, delete all the combinations that we
%first collected in that layer and only look for more combinations that fit
%higher threshold. 
%However, this may add too many vectors, so in subsequent layers, maybe
%do not add a combination unless it contributes significantly to the score 

%this is greedy approach, first assume its 1 component, then if it cant
%find a match 2 components, then 3 etc.
%want to detect up to 5 component mixture
%mixture has dimension 461-11=450
%solves the inverse problem of finding out which pure chemicals made mixture
%tests correlation with least squares residual
%||r||/||b||=||b-Ax||/||b||<0.05
% for now assume mixture is made of n<=5 pure chemical
n=5;
%mixlisttry has 5 layers because we are assuming mixture made up of <=5
%elements
%the xth layer of mixlisttry has 2x+1 columns.
%The first x are for each of the vectors in a possible combination of vectors
%THe next x are for the weights of the guessed components
%The 2x+1th column records the score=||projection of mixture onto span(first x vectors)||/||mixture||
%The number of rows in the xth layer depends on how stringent the thresholds are for that layer
tree={[],[],[],[],[]};
%top row of thresholds is lower bound for a combination to be accepted as a
%plausible component
%bottom row is upper bound to know when to quit
%0.5=cos(120/2), so assuming mixture is <=2 components, should succeed
%whenever the component vectors are at less than 120 degrees to each other
%-1 means not used, no meaning
%the first lower threshold must be considerably lower than the rest to give
%a starting layer to build possible combinations out of. If the starting
%lower threshold is too strict, algorithm may not find any combinations at
%all
%thresholds={[0.5;0.95],[0.9;0.97],[0.95;0.98],[0.97;0.99],[-1;0.999]};
%lower threshold of layer 1 we make as 0 to guarantee that at least one of
%the components of mixture will be captured
thresholds={[0.5;0.95],[0.9;0.97],[0.95;0.98],[0.97;0.99],[-1;0.999]};
%done will only be true if score=1
done=0;
%x indexes through all n layers of the tree
x=1;
l=norm(mixture);
while(x<=n && ~done)
    %hope-meaning that a promising subset of mixture has been found, 0=false, 1=true
    hope=0;
    if(x==1)
        y=2;
        while(y<=1150 && ~done)
            %score=cos(angle between the yth component from Narcotic Library and
            %mixture), which could be negative if the angle is obtuse
            score=transpose(NL2ndD_Nnormed(12:461,y))*mixture/l;
            if(score>0.9999)
                %we put 1 as the 2nd entry because since there is only 1
                %component, the weight of that component is 1
                tree{1}=[y,1,score];
                done=1;
            elseif(~hope)
                if(score>=thresholds{1}(1,:) && score<thresholds{1}(2,:))
                    %add possible 1st component to 1st layer with score which
                    %should be >=0.95
                    tree{1}=[tree{1};[y,1,score]];
                elseif(score>=thresholds{1}(2,:))
                    %once a vector above higher threshold is found,
                    %forget about the previous vectors found in
                    %tree{1}, the 1st layer. This is done by not
                    %including tree{1} in the assignment operation
                    %tree{1}<-stuff
                    tree{1}=[y,1,score];
                    hope=1;
                end
            elseif(score>=thresholds{1}(2,:))
                tree{1}=[tree{1};[y,1,score]];
            end
            y=y+1;
        end
    %if x>1
    else
        %y runs through the previous layer of mixlisttry and tests new
        %combinations with each of the 1149 chemicals added to the yth
        %previous combination
        y=1;
        prevsize=size(tree{x-1},1);
        while(y<=prevsize && ~done)
            %for each y, z runs through all 1149 chemicals to find possible
            %next candidates to add to the yth combination of x-1 vectors
            z=2;
            %need floor((x-1)/2) because the first layer has
            %[v1,weight,score]=3 entries, 2nd layer has [v1,v2, w1, w2,
            %score]=5 entries, 3rd layer has [v1,v2,v3,w1,w2,w3,score]=7
            %entries etc. so the xth layer has 2x+1 column entries
            prevlist=tree{x-1}(y,1:(x-1));
            while(z<=1150 && ~done)
                %only test the zth chemical if it is not in the yth combination
                %of the previous x-1 layer of the tree
                if(~ismember(z,prevlist))
                    A=NL2ndD_Nnormed(12:461,[prevlist,z]);
                    v=A\mixture;
                    score=norm(A*v)/l;
                    %v represents the weights of the vectors of A needed to produce projection of mixture onto col(A). Since we know
                    %the actual weights are positive, need to make sure all entries
                    %of v are positive
                    allpos=v<0;
                    if(sum(allpos)<=0)
                        if(score>0.9999)
                            tree{x}=[prevlist,z,transpose(v),score];
                            done=1;
                        %add possible 1st x components to xth layer of tree
                        %the last layer of tree is treated differently because
                        %there are no more layers, so there is no lower
                        %threshold by which the algorithm judges which
                        %combination of n vectors could be part of the mixture
                        elseif(x==n)
                            if(score>=thresholds{x}(2,:))
                                %add another possible subset of mixlist
                                %transpose(v) are the predicted weights
                                tree{x}=[tree{x};[prevlist,z,transpose(v),score]];
                            end
                        else
                            if(~hope)
                                if(score>=thresholds{x}(1,:) && score<thresholds{x}(2,:))
                                    %add another possible subset of mixlist
                                    tree{x}=[tree{x};[prevlist,z,transpose(v),score]];
                                elseif(score>=thresholds{x}(2,:))
                                    %once a vector above higher threshold is found,
                                    %forget about the previous vectors found in
                                    %tree{1}, the 1st layer. This is done by not
                                    %including tree{1} in the assignment operation
                                    %tree{1}<-stuff
                                    tree{x}=[prevlist,z,transpose(v),score];
                                    hope=1;
                                end
                            elseif(score>=thresholds{x}(2,:))
                                tree{x}=[tree{x};[prevlist,z,transpose(v),score]];
                            end
                        end
                    end
                end
                z=z+1;
            end
            y=y+1;
        end
    end
    x=x+1;
end
%minus 1 from x to accurately tell mixdecomptest where mixdecomp thought it
%found a match
%I made backwards while loop because if the thresholds are too stringent,
%the tree may be empty after a certain layer because the algorithm didn't
%find any combinations that have high enough threshold
x=5;
while(size(tree{x},1)==0)
    x=x-1;
end
mixlisttry=tree{x}(size(tree{x},1),:);
%at this point, mixlisttry has 2x+1 entries, the first x are the component
%indicies, the next x are the coefficients for the components, and the last
%entry is the score
%get rid of components with negligible coefficients
comps=mixlisttry(1:x);
weights=mixlisttry(x+1:2*x);
%find out which components in mixlisttry have negligible contribution,
%which is measured by weight <0.01
weights=weights/sum(weights);
%next 3 lines calculate projection percentage of only the most significant
%components, given by comps(weights>0.01)
A=NL2ndD_Nnormed(12:461,comps(weights>0.01));
v=A\mixture;
score=norm(A*v)/l;
%update mixlisttry to contain only the significant components, their new
%weights represented by v(which may have changed a little bit due to omitting the
%negligible vectors), and the new score (which still should be above
%0.9999)
mixlisttry=[comps(weights>0.01),transpose(v),score];
end