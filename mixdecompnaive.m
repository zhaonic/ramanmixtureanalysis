function [mixlisttry] = mixdecompnaive(mixture, NL2ndD_Nneg)
%mixture has dimension 461-11=450
%solves the inverse problem of finding out which pure chemicals in what
%concentrations made mixture
%tests correlation with least squares residual
%||r||/||b||=||b-Ax||/||b||<0.05
% for now assume mixture is made of n=2 pure chemical
n=2;
mixlisttry=[1,2];
A=zeros(450,n);
%done, 0=false, 1=true
done=0; 
%x indexes through mixlisttry
x=n;
%at the start of each iteration of the while loop, x should equal n
while(mislisttry~=(1149-n+1):1149 && ~done)%mixlisttry=(1149-n+1):1149 is the last combination
    %for example, in the combinations of ABCDEF choose 3, DEF is last
    %combination
    A=NarcoticLibrary(12:461,mixlisttry+1);
    v=A\mixture;
    score=norm(mixture-A*v);
    if(score==0)
        done=1;
    end
    %entire next section dedicated to updating to the next combination
    if(mixlisttry(x)>=1149)
        x=x-1;
        while(mixlisttry(x)>=mislisttry(x+1)-1)%the previous element is to the immediate left 
            %of the xth element, meaning it cannot be incremented further
            x=x-1;
        end            
    while(1149-mixlisttry(x)<=n-x)%if the xth entry in
        %mixlisttry reaches its rightmost allowed position in 1:1149
        %we are trying to loop through the 1149 C 2 combinations in
        %lexographic order
                
end
end

