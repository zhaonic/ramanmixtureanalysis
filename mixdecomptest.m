function [results,wrongs,testtime] = mixdecomptest(n,N,NL2ndD_Nnormed,NarcoticLibrary,upto)
%note that the indicies of mixlist and mixlisttry are 1 more than the
%original NL2ndD_Nnormed indices, i.e. if mixlist=[4,50,100], the actual
%components in Narcotic Library are [3,49,99]
%N is number of tests
%n=1:5, maximum number of components in mixture
%upto=1 if we want to test 1,2, and 3-component mixtures, =0 if we only
%want to test 3-component mixtures
%testtime is the total time taken to solve all N tests, should be less than
%time taken to run the entire test including creating results, deciding if
%good enough etc.
results=cell(N+1,5);
results{1,1}='right/wrong';
results{1,2}='real mix';
results{1,3}='real weights';
results{1,4}='top mix with weights and score';
results{1,5}='good enough?';
results{1,6}='time to create';
results{1,7}='time to solve';
wrongs=0;
testtime=0;
%for Savitsky Golay framelength=15, degree=3
frame=15;
deg=3;
[~,g]=sgolay(deg,frame);
for x=2:N+1
    %k is random integer from 1,...,n
    if(upto)
        k=ceil(rand*n);
    else
        k=n;
    end
    tic;
    [mixture,mixlist,weights]=randmix(k,NarcoticLibrary,g);
    results{x,6}=toc;
    %temporary for debugging
    %mixlist=[150,221,364];
    %weights=[0.4903;0.3739;0.1358];
    %mixture=NL2ndD_Nnormed(12:461,mixlist)*weights;
    %temporary
    tic;
    mixlisttry=mixdecompnoise(mixture,NL2ndD_Nnormed);
    results{x,7}=toc;
    testtime=testtime+results{x,7};
    results{x,2}=mixlist;
    results{x,3}=weights;
    results{x,4}=mixlisttry;
    %need floor(size(mixlisttry,2)/2) because for example, the 3rd layer
    %has [v1,v2,v3,w1,w2,w3,score]=7 entries, the xth layer has 2x+1
    %entries
    numofcomps=floor(size(mixlisttry,2)/2);
    %need to sort mixlisttry because the isequal function only considers 2
    %arrays equal if they have same elements in the same order
    predict=sort(mixlisttry(1:numofcomps));
    if(isequal(predict,mixlist))
        results{x,1}='yes';
    else
        results{x,1}='no';
        %next if block test whether mixlist and predict are "close
        %enough", meaning predict has more than 1/2 of the original
        %components of mixlist
        %Note: for n=3, often the close enough cases will differ from the answer by
        %the component that has the middle or smallest weight
        rights=sum(ismember(predict,mixlist));
        %score is the percentage of correct chemicals in predict
        score=rights/size(mixlist,2);
        wrongs=wrongs+(1-score);
        if(score>=0.5)
            results{x,5}='yes';
        else
            results{x,5}='no';
        end
    end
end
end
