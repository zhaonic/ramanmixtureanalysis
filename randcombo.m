function [combo] = randcombo(n)
%selects a random list of n indices representing 
%compounds without repetition from
%NarcoticLibrary2ndD_N in array form
%this function is needed because NarcoticLibrary2ndD_N is too large
%There are 1149 pure chemicals
combo=1:n;
r=round(rand*(1149-n));
for x=n:-1:1
    xnew=x+r;
    combo(x)=xnew;
    r=round(rand*r);
end
end

