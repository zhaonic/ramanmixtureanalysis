%This script is simply meant to test the normrnd function for mu=100 and
%sigma=sqrt(100)=10, 4 std devs on each side, so [60,140]
%N=number of times we pick a sample from the normrnd function
N=100000;
mu=100;
sigma=sqrt(mu);
x=zeros(1,N);
for i=1:N
    x(i)=round(normrnd(100,10));
end
histogram(x);