function [closecomps] = closecomps(NL2ndD_Nnormed,comp,tol)
%use dot product to give a list, called closecomps, of vectors closely
%related (dot product of vectors in closecomps with comp is more than tol) to comp
closecomps=[];
covector=transpose(comp);
for i=2:1150
    corr=covector*NL2ndD_Nnormed(12:461,i);
    if(corr>=tol)
        closecomps=[closecomps,i];
    end
end
end

