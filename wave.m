function wave(n,k,T,flag,test)
% function wave(n,k,T,flag,test)
%
% Solve the equation
%  d^2u       d^2u
%  ---- - c^2 ---- = 0   0<x<1, 0<t<T.
%  dt^2       dx^2
%
%  with IC and BC:
% ---------------------
%  TEST 1
%
%  u = 0 on boundary
%  u = 9.45*x^3(1-x) when t=0
%  du/dt = 0 when t=0
% ---------------------
%  TEST 2
%
%  u = 0 on boundary
%  u = 1 for x<.5, -1 for x>.5, when t=0
%  du/dt = 0 when t=0
% ---------------------
%  TEST 3
%
%  u = 0 on boundary
%  u = 0, when t=0
%  du/dt = 2*pi*sin(pi*x) when t=0
% ---------------------
%
% The finite difference method becomes
%
%  u^{j+1}_i = 2(1-sigma^2)u^j_i + sigma^2(u^j_{i+1} + u^j_{i-1}) - u^{j-1}_i
%
%  sigma = c k/h, h=1/n
% Stable iff
%  k <= 1/(sigma*n)
% Here
%  c = 1 = square root of the coefficient in the equation
%  n = number of space subintervals (h=1/n)
%  k = time step size
%  T = final time
%  flag = type of run
%    0 = normal
%    1 = just show final result
%
% First step:
%     u^1_i = (1 - sigma^2)*u^0_i + (sigma^2/2)*(u^0_{i+1} + u^0_{i-1})
c = 1;
if (test==3) c = 2; end
h = 1/n;
x = 0:h:1;







sigma2 = (c*k/h)^2;
t = 0;
step = 0;
if k > 1/(c*n)
s = 'CONDITION VIOLATED'
else
s = 'CONDITION SATISFIED'
end
v = zeros(1,n+1);  % initial condition
u = zeros(1,n+1);  % solution at time t_{j+1}
u1 = zeros(1,n+1); % solution at time t_{j}
u0 = zeros(1,n+1); % solution at time t_{j-1}
if (test==1)
init = @(x) 9.45*x.^3.*(1-x);
init_t = @(x) 0;
elseif (test==2)
init = @(x) (x<.5)-(x>=.5);
init_t = @(x) 0;
elseif (test==3)
init = @(x) 0;
init_t = @(x) 2*pi*sin(pi*x);  % u_t(x,0)
else
disp('Wrong test number');
return;
end
v(2:n) = init(x(2:n));
u = v;
u0 = v;
for i=2:n
u1(i) = (1 - sigma2)*v(i) + (sigma2/2)*(v(i+1) + v(i-1)) ...
+ k*init_t(x(i));
end
% Plot the solution
skip = 2;  % plot every 'skip' step
fh = figure;
set(fh, 'color', 'white');
hp = plot(NaN, NaN,'linewidth',2);
% grid on;
axis([0,1,-1,1]);
h=gca;
set(h,'FontSize',14);
xlabel('X axis');
ylabel('Solution');
%filename = 'sol_wave.gif';
filename = ['sol_wave_',num2str(test),'.gif'];
% loopcnt = inf;
loopcnt = 0;







if flag == 0
set(hp, 'XData', x, 'YData', u);
drawnow
str = ['TIME STEP = ',num2str(step), ' TIME = ',num2str(t)];
title(str);
F=getframe(fh);
im = frame2im(F);
[imind,cm] = rgb2ind(im,16);
imwrite(imind,cm,filename,'gif', 'writemode','overwrite', ...
'Loopcount',loopcnt,'DelayTime',0);
end
while t < T
t = t + k;
step = step + 1;
for i=2:n
u(i) = 2*(1-sigma2)*u1(i) + sigma2*(u1(i+1) + u1(i-1)) - u0(i);
end
u0(2:n)=u1(2:n);
u1(2:n)=u(2:n);
if ((flag == 0 && mod(step,skip)==0) || t >= T)
set(hp, 'XData', x, 'YData', u);
drawnow
str = ['TIME STEP = ',num2str(step), ' TIME = ',num2str(t)];
title(str);
F=getframe(fh);
im = frame2im(F);
[imind,cm] = rgb2ind(im,16);
imwrite(imind,cm,filename,'gif','WriteMode','append',...
'Loopcount',loopcnt,'DelayTime',0);
if(t==0.75)
    'u at x=1/4, t=3/4'
    u(n/4+1)
    'error at x=1/4, t=3/4'
    abs(sin(pi/4)*sin(3*pi/2)-u(n/4+1))
end
end
end